#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "service_events.h"

#define SERVICE_PORT			8081
#define SERVICE_MAX_CLIENTS		10

int main(int argc, char *argv[])
{
	socket_server_handle_t handle_sv;

	// Initiate the server
	handle_sv = socket_server_connection_create(SERVICE_MAX_CLIENTS, SERVICE_PORT);

	// Register the event handlers for the server
	register_events(handle_sv);

	while(1);

	return 0;
}
