#ifndef SERVICE_EVENTS_H_
#define SERVICE_EVENTS_H_

#include "sal/socket_server_subscriber.h"
#include "sal/socket_server.h"

int event_client_sent_message(void *instance, socket_server_client_t *client, char *message, size_t len);
int event_client_connected(void *instance, socket_server_client_t *client);
void register_events(socket_server_handle_t server);

#endif
