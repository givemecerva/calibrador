#include "tbl_configuracoes.h"

#include <stdio.h>
#include <iostream>
#include <sstream>

#include "database.h"

using namespace std;


TblConfiguracoes::TblConfiguracoes(Database *database)
{
	this->database = database;
}

int TblConfiguracoes::set(const char *parameter, const char *value)
{
	ostringstream query_string;

	query_string << "UPDATE configuracoes SET valor= '" << value << "' WHERE nome = '" << parameter << "'";
	return this->database->query(query_string.str().c_str());

}

char *TblConfiguracoes::get(const char *parameter)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW query_row;

	query_string << "SELECT valor FROM configuracoes WHERE nome= '" << parameter << "'";
	query_result = this->database->query_select(query_string.str().c_str());

	if(query_result != NULL){
		query_row = mysql_fetch_row(query_result);
		if (query_row == NULL) return NULL;

		return query_row[0];
	}else{
		std::cout << "Falha query";
		return NULL;
	}
}

int TblConfiguracoes::setLogo(const char *logo)
{
	return this->set("estabelecimento_logo", logo);
}

char *TblConfiguracoes::getLogo()
{
	return this->get("estabelecimento_logo");
}

int TblConfiguracoes::setNomeEstabelecimento(const char *name)
{
	return this->set("estabelecimento_nome", name);
}

char *TblConfiguracoes::getNomeEstabelecimento()
{
	return this->get("estabelecimento_nome");
}

int TblConfiguracoes::setAlgoritmoIA(const char *nro)
{
	return this->set("ia", nro);
}

char *TblConfiguracoes::getAlgoritmoIA()
{
	return this->get("ia");
}
