#ifndef DB_TBL_MOSTRADOR_H_
#define DB_TBL_MOSTRADOR_H_

#include "database.h"
#include "model.h"

enum{
	TBL_MOSTRADOR_E_SUCCESS,
	TBL_MOSTRADOR_E_FAILED_TO_INSERT_TOKEN,
	TBL_MOSTRADOR_E_FAILED_TO_REMOVE_TOKEN,
	TBL_MOSTRADOR_E_FAILED_TO_UPDATE_TOKEN
};

class TblMostrador{

private:
	Database *database;

public:
	TblMostrador(Database *database);
	int insertToken(token_t *token);
	int removeToken(token_t *token);
	int updateToken(token_t *token);
	bool exists(const char *mac);
};


#endif
