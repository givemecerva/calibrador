#include "tbl_maquinas.h"


using namespace std;

TblMaquinas::TblMaquinas(Database *database)
{
	this->database = database;
}


int TblMaquinas::recoverToken(const char *mac, token_t *token)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW row;

	query_string << "SELECT maquinas.id, maquinas.mac, maquinas.mesa, maquinas.fk_produto_id, maquinas.coeff_a, maquinas.coeff_b, ";
	query_string << "produtos.nome, produtos.vazio, produtos.cheio, produtos.logo, produtos.limiar_alerta, ";
	query_string << "comandas.abertura ";
	query_string << "FROM produtos ";
	query_string << "INNER JOIN maquinas ";
	query_string << "ON maquinas.fk_produto_id=produtos.id ";
	query_string << "INNER JOIN comandas ";
	query_string << "ON maquinas.mesa=comandas.mesa ";
	query_string << "WHERE maquinas.mac = '" << mac << "'";

	query_result = this->database->query_select(query_string.str().c_str());

	if(!query_result)
	{
		strcpy(token->begin_time, "");
		strcpy(token->logo, "");
		strcpy(token->mac_address, "");
		strcpy(token->product, "");
		token->id = 0;
		token->product_id = -1;
		token->isEmpty = false;
		token->table = 0;

		return TBL_MAQUINAS_TOKEN_RECOVER_FAIL;

	}else{

		row = mysql_fetch_row(query_result);
		if (row != NULL)
		{
			strcpy(token->mac_address, mac);
			token->id = atoi(row[0]);
			token->table = atoi(row[2]);
			token->product_id = atoi(row[3]);
			token->coeff_a = atof(row[4]);
			token->coeff_b = atof(row[5]);

			strcpy(token->product, row[6]);
			token->weigth_empty = atof(row[7]);
			token->weigth_full = atof(row[8]);

			strcpy(token->logo, row[9]);

			token->weigth_threshold = atof(row[10]);

			strcpy(token->begin_time, row[11]);

			return TBL_MAQUINAS_TOKEN_RECOVER_SUCESS;
		}
		else
		{
			return TBL_MAQUINAS_TOKEN_RECOVER_FAIL;
		}

	}

}

bool TblMaquinas::belongsToEstablishment(const char *mac)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW row;

	query_string << "SELECT * FROM maquinas WHERE mac='" << mac << "'";

	query_result = this->database->query_select(query_string.str().c_str());
	row = mysql_fetch_row(query_result);

	if(row == NULL)
	{
		return false;
	}else
	{
		return true;
	}

}

bool TblMaquinas::returnTokenByMac(const char *mac)
{
	ostringstream query_string;

	query_string << "UPDATE maquinas ";
	query_string << "SET mesa = '-1' ";
	query_string << "WHERE mac= '" << mac << "'";

	return this->database->query(query_string.str().c_str());

}

int TblMaquinas::tokensInTable(const int table)
{
	ostringstream query_string;
	MYSQL_RES *query_result;

	query_string << "SELECT * FROM maquinas WHERE mesa = '" << table << "'";

	query_result = this->database->query_select(query_string.str().c_str());

	return mysql_num_rows(query_result);

}

int *TblMaquinas::returnIdAvaiableTokens()
{
	return 0;
}

int TblMaquinas::getAvaiableTokensQuantity()
{
	MYSQL_RES *query_result;

	query_result = this->database->query_select("SELECT * FROM maquinas WHERE mesa = '-1'");

	return mysql_num_rows(query_result);
}

int TblMaquinas::updateMachine(table_t table)
{
	ostringstream query_string;

	query_string << "UPDATE maquinas SET mesa = '" << table.table_number << "', fk_produto_id = '";
	query_string << table.product << "' WHERE id = '" << table.id_token << "'";

	return this->database->query(query_string.str().c_str());
}

int TblMaquinas::returnTokenById(const int token_id)
{
	ostringstream query_string;

	query_string << "UPDATE maquinas SET mesa = '-1' WHERE id = '" << token_id << "'";

	return this->database->query(query_string.str().c_str());

}

bool TblMaquinas::verifyRemove(const int token_id)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW row;

	query_string << "SELECT mesa FROM maquinas WHERE id ='" << token_id << "'";
	query_result = this->database->query_select(query_string.str().c_str());

	row = mysql_fetch_row(query_result);

	if (row == NULL) return false;

	if(atoi (row[0]) == -1){

		return true;

	}else{

		return false;

	}
}

int TblMaquinas::getTableByID(const int token_id, table_t *table)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW row;

	query_string << "SELECT * FROM maquinas WHERE id = '" << token_id << "'";
	query_result = this->database->query_select(query_string.str().c_str());

	if(query_result){

		row = mysql_fetch_row(query_result);
		if (row == NULL) return TBL_MAQUINAS_GET_TABLE_FAIL;

		table->id_token = atoi(row[0]);
		table->table_number = atoi(row[2]);
		table->product = atoi(row[3]);

		return TBL_MAQUINAS_GET_TABLE_SUCESS;

	}else{

		table->id_token = 0;
		table->product = 0;
		table->table_number = 0;

		return TBL_MAQUINAS_GET_TABLE_FAIL;
	}
}

