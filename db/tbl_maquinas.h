#ifndef DB_TBL_MAQUINAS_H_
#define DB_TBL_MAQUINAS_H_

#include <mysql/mysql.h>

#include "database.h"
#include "model.h"

enum{
	TBL_MAQUINAS_TOKEN_RECOVER_SUCESS,
	TBL_MAQUINAS_TOKEN_RECOVER_FAIL,
	TBL_MAQUINAS_GET_TABLE_SUCESS,
	TBL_MAQUINAS_GET_TABLE_FAIL
};


class TblMaquinas
{
private:
	Database *database;

public:
	TblMaquinas(Database *database);
	//token_t recoverToken(const char *mac, const int value);
	int recoverToken(const char *mac, token_t *token);
	bool belongsToEstablishment(const char *mac);
	bool returnTokenByMac(const char *mac);
	int tokensInTable(const int table);
	int *returnIdAvaiableTokens();
	int getAvaiableTokensQuantity();
	int updateMachine(table_t table);
	int returnTokenById(const int token_id);
	bool verifyRemove(const int token_id);
	//table_t getTableByID(const int token_id);
	int getTableByID(const int token_id, table_t *table);

};


#endif /* DB_TBL_MAQUINAS_H_ */
