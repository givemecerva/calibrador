#ifndef H_TBLPRODUTOS
#define H_TBLPRODUTOS

#include <sstream>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mysql/mysql.h>

#include "database.h"
#include "model.h"

enum
{
	TBL_PRODUTOS_E_SUCCESS,
	TBL_PRODUTOS_E_PRODUCT_NOT_FOUND
};

class TblProduto
{
private:
	Database *database;

public:
	TblProduto(Database *database);
	int getProductByID(int id, product_t *product);
};

#endif
