#include "database.h"

Database::Database(const char *host, const char *user, const char *password, unsigned int port, const char *bd)
{
	strcpy(this->host, host);
	strcpy(this->user, user);
	strcpy(this->password,password);
	strcpy(this->bd, bd);
	this->port = port;
}


int Database::connect()
{
	mysql_init(&(this->connection));

	if(!mysql_real_connect(&(this->connection), this->host, this->user, this->password, this->bd, this->port, NULL, 0))
	{
	   std::cout << "Error (" << mysql_errno(&connection) << ") = " << mysql_error(&connection) << std::endl;
	   return DATABASE_E_FAIL_CONNECT;
	}
	return DATABASE_E_SUCCESS;
}

int Database::disconnect()
{
	mysql_close(&(this->connection));

	return DATABASE_E_SUCCESS;
}

int Database::query(const char *query_str)
{
	if (mysql_query(&(this->connection), query_str))
	{
		std::cout << "Failed to execute the following SQL Query: "<< query_str << std::endl;
		std::cout << "Error (" << mysql_errno(&connection) << ") = " << mysql_error(&connection) << std::endl;
		return DATABASE_E_FAIL_CONSULT;
	}

	return DATABASE_E_SUCCESS;
}

MYSQL_RES *Database::query_select(const char *query_str)
{
	if(mysql_query(&(this->connection), query_str))
	{
		std::cout << "Erro %d: %s\n", mysql_errno(&(this->connection)), mysql_error(&(this->connection));
		return NULL;

	}else{

		return mysql_store_result(&(this->connection));
	}
}

