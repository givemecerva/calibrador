#ifndef DB_TBL_CONFIGURACOES_H_
#define DB_TBL_CONFIGURACOES_H_

#include <mysql/mysql.h>
#include "database.h"

class TblConfiguracoes
{

private:
	Database *database;

	//de onde vem parametro e valor? qual o modelo?
public:
	TblConfiguracoes(Database *database);
	int set(const char *parameter, const char *value);
	char *get(const char *parameter);
	int setLogo(const char *logo);
	char *getLogo();
	int setNomeEstabelecimento(const char *name);
	char *getNomeEstabelecimento();
	int setAlgoritmoIA(const char *nro);
	char *getAlgoritmoIA();


};

#endif /* DB_TBL_CONFIGURACOES_H_ */
