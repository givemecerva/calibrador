#ifndef DB_DATABASE_H_
#define DB_DATABASE_H_

#include <mysql/mysql.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>

enum {
	DATABASE_E_SUCCESS,
	DATABASE_E_FAIL_CONNECT,
	DATABASE_E_FAIL_CONSULT
};

class Database{

private:
	char bd[256];
	char host[256];
	char user[64];
	char password[64];
	unsigned int port;
	MYSQL connection;


public:
	Database(const char *host,
		  const char *user,
		  const char *password,
		  unsigned int port,
		  const char *bd);

	int connect();
	int disconnect();
	int query(const char *query_str);
	MYSQL_RES *query_select(const char *query_str);

};

#endif
