#include "tbl_produto.h"

using namespace std;

TblProduto::TblProduto(Database *database)
{
	this->database = database;
}

int TblProduto::getProductByID(int id, product_t *product)
{
	ostringstream query_string;
	MYSQL_RES *query_result;
	MYSQL_ROW row;

	query_string << "SELECT * FROM produtos WHERE id = '" << id << "'";

	query_result = this->database->query_select(query_string.str().c_str());

	if(!query_result)
	{
		return TBL_PRODUTOS_E_PRODUCT_NOT_FOUND;
	}else{
		row = mysql_fetch_row(query_result);
		if (row == NULL) return TBL_PRODUTOS_E_PRODUCT_NOT_FOUND;

		product->id = atoi(row[0]);
		strcpy(product->name, row[1]);
		product->empty_weight = atof(row[2]);
		product->full_weigth = atof(row[3]);
		strcpy(product->logo, row[4]) ;
		product->alert_threshold = atof(row[5]);

		return TBL_PRODUTOS_E_SUCCESS;
	}
}
