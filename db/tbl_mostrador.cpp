#include "tbl_mostrador.h"

#include <iostream>
#include "model.h"
#include <sstream>
#include "database.h"

using namespace std;

TblMostrador::TblMostrador(Database *database)
{
	this->database = database;
}

int TblMostrador::insertToken(token_t *token)
{
	ostringstream query_string;

	query_string << "INSERT INTO mostrador (mac, valor) ";
	query_string << "VALUES('" << token->mac_address << "', '" << token->value <<"')";

	if (this->database->query(query_string.str().c_str()))
	{
		memset(token, 0, sizeof(token_t));

		return TBL_MOSTRADOR_E_FAILED_TO_INSERT_TOKEN;
	}

	return TBL_MOSTRADOR_E_SUCCESS;
}

int TblMostrador::removeToken(token_t *token)
{
	ostringstream query_string;

	query_string << "DELETE FROM mostrador WHERE mac='";
	query_string << token->mac_address << "'";

	if(this->database->query(query_string.str().c_str()))
	{
		return TBL_MOSTRADOR_E_FAILED_TO_REMOVE_TOKEN;
	}

	return TBL_MOSTRADOR_E_SUCCESS;
}

int TblMostrador::updateToken(token_t *token)
{
	ostringstream query_string;

	query_string << "UPDATE mostrador SET valor = " << token->value << " WHERE mac=\"" << token->mac_address << "\"";

	if (this->database->query(query_string.str().c_str()))
	{
		return TBL_MOSTRADOR_E_FAILED_TO_UPDATE_TOKEN;
	}

	return TBL_MOSTRADOR_E_SUCCESS;
}

bool TblMostrador::exists(const char *mac)
{
	ostringstream query_string;
	MYSQL_RES *query_result;

	query_string << "SELECT * FROM mostrador WHERE mac='" << mac << "'";
	query_result = this->database->query_select(query_string.str().c_str());

	if(mysql_num_rows(query_result) == 0){
		return false;
	}else{
		return true;
	}


}
