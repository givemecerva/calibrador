/*
 * model.h
 *
 *  Created on: May 22, 2016
 *      Author: hugo
 */

#ifndef MODEL_H_
#define MODEL_H_

typedef struct
{
	unsigned int id;
	unsigned int value;
	int table;
	double weigth_empty;
	double weigth_full;
	double weigth_threshold;
	unsigned int isEmpty;
	double coeff_a;
	double coeff_b;
	double percentage;
	char mac_address[20];
	char product[40];
	int product_id;
	char logo[256];
	char begin_time[500];
}token_t;

typedef struct
{
	int table_number;
	int product;
	int id_token;
}table_t;

typedef struct
{
	int id;
	char name[40];
	char email[40];
	char password[40];
	char login[40];
}user_t;

typedef struct
{
	int id;
	char name[40];
	double empty_weight;
	double full_weigth;
	char logo[256];
	double alert_threshold;
}product_t;

#endif /* MODEL_H_ */
