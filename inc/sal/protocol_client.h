#ifndef H_PROTOCOL_CLIENT
#define H_PROTOCOL_CLIENT

#include <stdint.h>
#include "protocol.h"

/* Create the packet for the notify command.
 *
 * mac_address = 6 bytes long
 * value = 4 bytes long
 */
uint32_t protocol_client_create_notify(uint8_t *mac_address, uint8_t *value, uint8_t *packet, size_t *packet_len);

/* Parse the data received as the notify response */
uint32_t protocol_client_parse_notify_response(uint8_t *packet, size_t packet_len, uint8_t *action);

/* Create the packet for the Get Operation Data command. */
uint32_t protocol_client_create_get_operation_data(uint8_t *mac_address, uint8_t *packet, size_t *packet_len);

/* Used to extract the operation data from the server response */
uint32_t protocol_client_parse_get_operation_data(uint8_t *packet, size_t packet_len,
												  uint32_t *alert_threshold,
												  uint32_t *minimum_weight,
												  uint32_t *refresh_rate,
												  uint32_t *timeout_counter);


/* Create the packet for the Get Calibration Data command. */
uint32_t protocol_client_create_get_calibration_data(uint8_t *packet, size_t *packet_len);

/* Used to extract the linear and angular coefficients */
uint32_t protocol_client_parse_get_calibration_data(uint8_t *packet, size_t packet_len,
											   float *m, float *n);


#endif
