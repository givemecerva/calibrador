#ifndef H_LINKED_LIST
#define H_LINKED_LIST

#ifdef __cplusplus
extern "C" {
#endif

/* Error Codes */
enum{
	LINKED_LIST_STATUS_SUCCESS = 0,
	LINKED_LIST_STATUS_GENERIC_ERROR,
	LINKED_LIST_STATUS_ELEMENT_NOT_FOUND,
	LINKED_LIST_STATUS_COMPARISON_FAILED
};


typedef void* linked_list_t;
typedef int (*compareMethod)(void *a, void *b, unsigned int memsize);
typedef void (*printMethod)(void *a);

/*
 * Allocates a new linked list and returns the instance pointer
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 		In case of success
 * 		LINKED_LIST_STATUS_GENERIC_ERROR	Otherwise
 */
linked_list_t linked_list_create();

/*
 * Adds a new item to the linked list
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 		In case of success
 * 		LINKED_LIST_STATUS_GENERIC_ERROR	Otherwise
 */
int linked_list_add(linked_list_t list, void *item, unsigned int item_len);

/*
 * Removes a item from the linked list
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 			In case of success
 * 		LINKED_LIST_STATUS_ELEMENT_NOT_FOUND	In case the request item was not found
 */
int linked_list_remove(linked_list_t list, void *item, unsigned int memsize);

/*
 * De-allocates and destroy the whole linked list
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 		In case of success
 * 		LINKED_LIST_STATUS_GENERIC_ERROR	Otherwise
 */
int linked_list_destroy(linked_list_t list);

/*
 * Specifies the method used to compare two items on the list.
 * This methods may return LINKED_LIST_STATUS_SUCCESSFULL if the two arguments
 * are equals (in value) or LINKED_LIST_STATUS_COMPARISON_FAILED if they are different.
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 		In case of success
 * 		LINKED_LIST_STATUS_GENERIC_ERROR	Otherwise
 */
int linked_list_set_compare_method(linked_list_t list, compareMethod method);

/*
 * This method is used to specify the method used to print each element of the linked list.
 *
 * Returns:
 *
 * 		LINKED_LIST_STATUS_SUCCESSFULL 		In case of success
 * 		LINKED_LIST_STATUS_GENERIC_ERROR	Otherwise
 */
int linked_list_set_print_method(linked_list_t list, printMethod method);

/*
 * This method returns the number of elements on the linked list
 */
int linked_list_length(linked_list_t list);

/*
 * This method returns the pointer to the item on the pos position
 */
int linked_list_get_item(linked_list_t list, int pos, void *item, int length);

#ifdef __cplusplus
}
#endif

#endif
