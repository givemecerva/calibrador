/*
 * socket_server_subscriber.h
 *
 *  Created on: Nov 29, 2015
 *      Author: user
 */

#ifndef INC_SAL_SOCKET_SERVER_SUBSCRIBER_H_
#define INC_SAL_SOCKET_SERVER_SUBSCRIBER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define SOCKET_SERVER_CLIENT_BUFFERS_SIZE 512

/*
 * This file defines the interface needed to implement the event-handlers: clientConnected and clientSentMessage.
 * Basically you need:
 *
 * 	- Create the methods following the signature below
 *
 * 	- Create a socket_server_observer_t structure and point the handler you just created
 *
 * 	- Register the structure on the list of events available on the server using the methods:
 * 	  socket_server_subscribe_client_connected or socket_server_subscribe_client_sent_message
 */

typedef struct
{
	void *connection;
	int fd;
	int id;
	struct sockaddr_in cli_addr;
	pthread_t thread;
	char recv_buffer[SOCKET_SERVER_CLIENT_BUFFERS_SIZE];
}socket_server_client_t;

typedef int (*eventHandler_clientConnected_t)(void *, socket_server_client_t *); // instance, client id and client ip address
typedef int (*eventHandler_clientSentMessage_t)(void *, socket_server_client_t *, char *, size_t); // instance, client, message

/* Observer Interface */
typedef struct{
	void *instance;
	union{
		eventHandler_clientConnected_t client_connected;
		eventHandler_clientSentMessage_t client_SentMessage;
	} handler;
} socket_server_observer_t;

#ifdef __cplusplus
}
#endif

#endif /* INC_SAL_SOCKET_SERVER_SUBSCRIBER_H_ */
