#ifndef H_MISC
#define H_MISC

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

#define DEBUG
#ifdef DEBUG
#	define TRACE(format, args...) printf ("[+] DEBUG (%s): "format"\n" , __func__, ##args)
#else
#	define TRACE(fmt, ... ) (void)0
#endif

#define LOG_CONSOLE(format, args...) printf ("[+] Log: "format"\n" , ##args)

#define checked_malloc(var, size) do{ \
							void *malloc_return; \
							malloc_return = malloc(size); \
							if (malloc_return == NULL){ \
								TRACE("<Fatal Error> Could not alloc..."); \
								exit(1); \
							} else { \
								var = malloc_return; \
							} \
							}while(0);

#define TO_LOWER_CASE(x, len) do{int i = 0; for(; i < len; i++) x[i] = (('A' <= x[i]) && (x[i] <= 'Z')) ? x[i] + 32 : x[i];}while(0)

#ifdef __cplusplus
}
#endif

#endif
