#ifndef H_PROTOCOL_SERVER
#define H_PROTOCOL_SERVER

#include "protocol.h"

/*
 * Generate the notify packet response according to the desired action.
 * Use the available actions: NOTIFY_ACTIONS_XXX
 */
int protocol_server_create_notify_response(uint8_t action, uint8_t *packet, size_t *packet_len);

/* Extracts the sensor read and the mac address from the bytes received from client */
int protocol_server_parse_notify(uint8_t *packet, size_t packet_len, unsigned int *sensor_value, char *mac_address_str);

/* Generate the Get Operation Data packet response
 *
 * alert_threshold (4 bytes) -> Under this level (in grams), the token shall wake up and notify the server
 * update_percentage (1byte) -> This is the minimum delta (in %) that may be sensed for generating a notification
 * refresh_rate (4 bytes) 	 -> Refresh rate (in milliseconds) between each sensor read.
*/
int protocol_server_create_get_operation_data_response(uint8_t *alert_threshold,
													   uint8_t *minimum_weight,
													   uint8_t *refresh_rate,
													   uint8_t *timeout_counter,
													   uint8_t *packet, size_t *packet_len);

int protocol_server_parse_get_operation_data(uint8_t *packet, size_t packet_len, char *mac_address_str);

/* Generate the Get Calibration Data packet response.
 *
 * y = f(x) = a.x + b
 *
 * angular coefficient (a) (4 bytes)
 * linear coefficient  (b) (4 bytes)
*/
int protocol_server_create_get_calibration_data_response(uint8_t *coeff_angular,
													   uint8_t *coeff_linear,
													   uint8_t *packet, size_t *packet_len);

#endif
