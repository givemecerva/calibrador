#ifndef H_SAL_SOCKET_SERVER
#define H_SAL_SOCKET_SERVER

#include "sal/socket_server_subscriber.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Error Codes */
enum{
	SOCKET_SERVER_STATUS_SUCCESS = 0,
};

typedef struct socket_server_handle_struct* socket_server_handle_t;

/*
	This file defines the software abstraction layer that will be used to access the socket.
*/

/*
	This method creates a server socket handling when receiving the connection parameters.
	
	It returns:

		SOCKET_SERVER_STATUS_SUCCESSFULL
		
*/
socket_server_handle_t socket_server_connection_create(int number_of_clients, int port);

/*
	This method closes an active connection.

	It returns:

		SOCKET_SERVER_STATUS_SUCCESSFULL
*/
int socket_server_connection_close(socket_server_handle_t connection);

/*
	This method sends a message to the client identified by the client_id argument

	It returns:

		SOCKET_SERVER_STATUS_SUCCESSFULL
*/
int socket_server_connection_send(socket_server_handle_t connection, const socket_server_client_t *client, const char *message, const size_t len);

int socket_server_get_port(socket_server_handle_t connection);

/*
	This method subscribe a client to the event of a client connection

	It returns:

		SOCKET_SERVER_STATUS_SUCCESSFULL
*/
int socket_server_subscribe_client_connected(socket_server_handle_t connection, socket_server_observer_t listener);


int socket_server_subscribe_client_sent_message(socket_server_handle_t connection, socket_server_observer_t listener);

#ifdef __cplusplus
}
#endif

#endif
