#ifndef H_PROTOCOL
#define H_PROTOCOL

#define PROTOCOL_MAX_PACKET_SIZE 	32
#define PROTOCOL_MAX_PAYLOAD_SIZE 	32

#include <stddef.h>
#include <stdint.h>

/* Error Codes */
enum{
	PROTOCOL_E_SUCCESS = 0x00,
	PROTOCOL_E_INVALID_CMD_ID,
	PROTOCOL_E_INVALID_EDC,
	PROTOCOL_E_INVALID_LENGTH,
	PROTOCOL_E_LOGIC,
	PROTOCOL_E_BUFFER_OVERFLOW,
	PROTOCOL_E_NO_PAYLOAD
};

/* Command IDs */
enum{
	CMD_ID_NOTIFY = 0x00,
	CMD_ID_GET_OPERATION_DATA,
	CMD_ID_GET_CALIBRATION_DATA,
	CMD_ID_NAK,

	CMD_ID_INVALID
};

/* Available Actions */
enum{
	NOTIFY_ACTIONS_OPERATION = 0x00,
	NOTIFY_ACTIONS_CALIBRATE,
	NOTIFY_ACTIONS_SLEEP,
	NOTIFY_ACTIONS_CONTINUE,

	NOTIFY_ACTIONS_INVALID
};


typedef unsigned char uint8_t;

static uint8_t packet_buffer[PROTOCOL_MAX_PACKET_SIZE];

/*
 * This function creates the packet on the following format:
 * [CMD_ID][Payload Length][Payload Byte 0][Payload Byte 1]...[Payload Byte n][EDC]
 */
uint32_t protocol_create_packet(uint8_t cmd_id, const uint8_t *payload, size_t payload_len, uint8_t *packet, size_t *packet_size);

/* This function "opens" a packet. It checks its integrity and returns the payload. */
uint32_t protocol_open_packet(uint8_t *bytes, size_t bytes_len, uint8_t *cmd_id, uint8_t *payload, size_t *payload_len);

/* Create the packet for the NAK. */
uint32_t protocol_create_nak(uint8_t *packet, size_t *packet_size);

/* Misc functions related to protocol */
void swap_bytes(uint8_t *address, size_t len);
uint32_t get_payload(uint8_t *packet, size_t packet_len, uint8_t **payload, size_t *payload_len);

#endif
