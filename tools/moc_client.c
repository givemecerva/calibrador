#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 8081
#define BYTESWAP 1
#define CMD_ID_NOTIFY 0

void swap_bytes(uint8_t *address, size_t len)
{
	size_t i;
	uint8_t tmp[len];

	memcpy(tmp, address, len);

	for(i = 0; i < len; i++) address[i] = tmp[len - i - 1];
}

uint32_t protocol_create_packet(uint8_t cmd_id, const uint8_t *payload, size_t payload_len, uint8_t *packet, size_t *packet_size)
{
	size_t i;
	uint8_t edc = 0x00;

	packet[0] = cmd_id;
	packet[1] = payload_len;
	memcpy(&packet[2], payload, payload_len);

	for(i = 0; i < payload_len + 2; i++)
	{
		edc ^= packet[i];
	}

	packet[payload_len +2] = edc;

	*packet_size = payload_len + 3;

	return 0;
}

uint32_t protocol_client_create_notify(uint8_t *mac_address, uint8_t *value, uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[10];
	int i = 0;

	//memcpy(payload, mac_address, 6);
	for(i = 0; i < 6; i++)
	{
		payload[i] = mac_address[5 - i];
	}

	if (BYTESWAP) swap_bytes((uint8_t *) value, 4);

	memcpy(&payload[6], value, 4);

	return protocol_create_packet(CMD_ID_NOTIFY, payload, 10, packet, packet_len);
}

int main(int argc, char *argv[])
{
	uint8_t packet[128];
	size_t packet_len;
	uint32_t weigth_in_grams = 0x00;
	uint32_t weigth_in_ad = 0x00;
	uint8_t mac_address[] = {0x7D, 0x2D, 0xFE, 0x34, 0xFE, 0x18};
	double coeff_a = 0.011764706;
	double coeff_b = -91156.23529;
	int i = 0;

	int sockfd = 0;
    struct sockaddr_in serv_addr; 

	if (argc < 2)
	{
		printf("Invalid arguments... \n");
		return -1;
	}

	weigth_in_grams = atoi(argv[1]);
	weigth_in_ad = (uint32_t) (weigth_in_grams - coeff_b)/coeff_a;
	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X \n", mac_address[0], mac_address[1], mac_address[2],
												  mac_address[3], mac_address[4], mac_address[5]
	);
	printf("Weigth in grams: %d \n", weigth_in_grams);
	printf("Value in AD: %d \n", weigth_in_ad);

	protocol_client_create_notify(mac_address, (uint8_t *) &weigth_in_ad, packet, &packet_len);
 
	printf("Packet bytes: ");
	for(i = 0; i < packet_len; i++)
	{
		printf("0x%02X ", packet[i]);
	}
	printf("\n");

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    } 

    memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT); 

    if(inet_pton(AF_INET, SERVER_IP, &serv_addr.sin_addr) <= 0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
       return 1;
    } 

    write(sockfd, packet, packet_len);

    if (argc > 2)
    {
    	if (!strcmp(argv[2], "keep")) while(1);	
    }
    


	return 0;
}