#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sal/protocol.h"
#include "sal/protocol_client.h"
#include "sal/protocol_server.h"

#include "sal/socket_server.h"
#include "sal/misc.h"

#include "db/database.h"
#include "db/tbl_maquinas.h"
#include "db/tbl_produto.h"
#include "db/tbl_configuracoes.h"
#include "db/tbl_mostrador.h"
#include "db/model.h"

#include "service_events.h"

#define OPERATION_DATA_UPDATE_PERCENTAGE                      1
#define OPERATION_DATA_REFRESH_RATE                           1000

void hexdump(uint8_t *array, size_t len)
{
	size_t i = 0;
	for(i = 0; i < len - 1; i++) printf("0x%02X ", array[i]);

	printf("0x%02X \n", array[i]);
}

static void token_print(token_t *token)
{
	printf("token->id = 0x%08X \n", token->id);
	printf("token->value = 0x%08X \n", token->value);
	printf("token->table = 0x%08X \n", token->table);

	printf("token->weigth_empty = %.2lf \n", token->weigth_empty);
	printf("token->weigth_full = %.2lf \n", token->weigth_full);
	printf("token->weigth_threshold = %.2lf \n", token->weigth_threshold);
	printf("token->coeff_a = %.8lf \n", token->coeff_a);
	printf("token->coeff_b = %.8lf \n", token->coeff_b);
	printf("token->percentage = %.8lf \n", token->percentage);

	printf("token->isEmpty = 0x%08X \n", token->isEmpty);
	printf("token->mac_address = %s \n", token->mac_address);
	printf("token->product = %s \n", token->product);
	printf("token->product_id = 0x%08X \n", token->product_id);
	printf("token->logo = %s \n", token->logo);
	printf("token->begin_time = %s \n", token->begin_time);
}

static void product_print(product_t *product)
{
	printf("product->id = 0x%08X \n", product->id);
	printf("product->name = %s \n", product->name);
	printf("product->empty_weight = %.8lf \n", product->empty_weight);
	printf("product->full_weigth = %.8lf \n", product->full_weigth);
	printf("product->logo = %s \n", product->logo);
	printf("product->alert_threshold = %.8lf \n", product->alert_threshold);
}

int event_client_sent_message(void *instance, socket_server_client_t *client, char *message, size_t len)
{
	Database *db;
	TblMaquinas *tblMaquinas;
	TblProduto *tblProdutos;
	TblConfiguracoes *tblConfiguracoes;
	TblMostrador *tblMostrador;
	token_t myToken;
	product_t myProduct;

	int rv = 0;
	char *ip_address;
	char *log_message;
	uint8_t *cmd_buffer, *packet;
	uint8_t cmd_id;
	int i = 0;
	unsigned int value;
	char mac_address[20];
	uint8_t packet_response[100];
	size_t packet_len;
	uint8_t action = NOTIFY_ACTIONS_SLEEP;
	unsigned int m = 1052672;
	unsigned int n = 268435472;

	cmd_buffer = (uint8_t *) message;

	ip_address = (char *) inet_ntoa((client->cli_addr).sin_addr);
	log_message = (char *) malloc( (len + 50)*sizeof(char));

	cmd_id = cmd_buffer[0];

	// TODO: decide which action to take
	db = new Database("localhost", "root", "43125", 3306, "givemecerva");
	if (rv = db->connect())
	{
		LOG_CONSOLE("Connection failed with error: %d \n", rv);
		return 0;;
	}

	tblMaquinas = new TblMaquinas(db);
	tblProdutos = new TblProduto(db);
	tblConfiguracoes = new TblConfiguracoes(db);
	tblMostrador = new TblMostrador(db);

	switch(cmd_id)
	{
		case CMD_ID_NOTIFY:
			double weigth_total;
			double current_weigth;
			uint8_t action;
			protocol_server_parse_notify(cmd_buffer, len, &value, mac_address);
			LOG_CONSOLE("Client sent notify: value = %d from MAC_ADDRESS: %s", value, mac_address);

			rv = tblMaquinas->recoverToken(mac_address, &myToken);
			LOG_CONSOLE("Recovered token..");
			if (rv != TBL_MAQUINAS_TOKEN_RECOVER_SUCESS)
			{
				LOG_CONSOLE("The mac address was not found on the db (%s) : %d \n", mac_address, rv);
				protocol_server_create_notify_response(NOTIFY_ACTIONS_SLEEP, packet_response, &packet_len);
				socket_server_connection_send((socket_server_handle_t) instance, client, (char *) packet_response, packet_len);
				break;
			}
			myToken.value = value;

			// Calculate the percentage
			current_weigth = myToken.coeff_a * value + myToken.coeff_b;
			weigth_total = myToken.weigth_full - myToken.weigth_empty;
			myToken.percentage = (current_weigth - myToken.weigth_empty)/weigth_total;
			myToken.percentage *= 100;

			LOG_CONSOLE("Token data extracted from the database: \n\n");
			token_print(&myToken);

			// Check if the token is associated to a table
			if (myToken.table < 0)
			{
				action = NOTIFY_ACTIONS_SLEEP;
				protocol_server_create_notify_response(action, packet_response, &packet_len);
				socket_server_connection_send((socket_server_handle_t) instance, client, (char *) packet_response, packet_len);
				return 0;
			}

			LOG_CONSOLE("current_weigth = %.8lf \n", current_weigth);
			if ((current_weigth > myToken.weigth_empty) && (current_weigth < ((myToken.weigth_threshold * myToken.weigth_full)/100)))
			{
				LOG_CONSOLE("This token measurement is under the weigth threshold, it must be displayed on screen! \n");
				action = NOTIFY_ACTIONS_OPERATION;
				protocol_server_create_notify_response(action, packet_response, &packet_len);
				socket_server_connection_send((socket_server_handle_t) instance, client, (char *) packet_response, packet_len);

				if (tblMostrador->exists(myToken.mac_address))
				{
					rv = tblMostrador->updateToken(&myToken);
					if (rv) LOG_CONSOLE("Failed to update token in the mostrador tbl: 0x%08X \n", rv);
				}
				else
				{
					rv = tblMostrador->insertToken(&myToken);
					if (rv) LOG_CONSOLE("Failed to insert token in the mostrador tbl: 0x%08X \n", rv);
				}

				LOG_CONSOLE("insertToken returned: 0x%08X \n", rv);
			}else
			{
				action = NOTIFY_ACTIONS_SLEEP;
				protocol_server_create_notify_response(action, packet_response, &packet_len);
				socket_server_connection_send((socket_server_handle_t) instance, client, (char *) packet_response, packet_len);

				// It is above the threshold and is being showed... must be removed!
				if (tblMostrador->exists(myToken.mac_address))
				{
					rv = tblMostrador->removeToken(&myToken);
					if (rv) LOG_CONSOLE("Failed to remove the token %s. Error code = 0x%08X \n", myToken.mac_address, rv);
				}
			}

			break;

		case CMD_ID_GET_OPERATION_DATA:
			{
				uint32_t weigth_threshold = 10000;
				uint32_t weigth_minimum = 2000;
				uint32_t refresh_rate = OPERATION_DATA_REFRESH_RATE;
				uint32_t timeout_counter = 10;
				char string_cfg_value[20];

				protocol_server_parse_get_operation_data(cmd_buffer, len, mac_address);
				LOG_CONSOLE("Get Operation Data: %s", mac_address);

				rv = tblMaquinas->recoverToken(mac_address, &myToken);
				LOG_CONSOLE("Recovered token..");
				if (rv != TBL_MAQUINAS_TOKEN_RECOVER_SUCESS)
				{
					LOG_CONSOLE("The mac address was not found on the db (%s) : %d \n", mac_address, rv);
					protocol_server_create_notify_response(NOTIFY_ACTIONS_SLEEP, packet_response, &packet_len);
					break;
				}

				LOG_CONSOLE("Querying for product with the following ID: %d", myToken.product_id);

				rv = tblProdutos->getProductByID(myToken.product_id, &myProduct);
				if (rv != TBL_PRODUTOS_E_SUCCESS)
				{
					LOG_CONSOLE("The product was not found...");
					break;
				}

				product_print(&myProduct);

				// Get percentage_update
				strcpy(string_cfg_value, tblConfiguracoes->get("taxa_atualizacao_em_ms"));
				refresh_rate = atoi(string_cfg_value);
				printf("refresh_rate = %s \n", string_cfg_value);
				printf("refresh_rate = %d \n", refresh_rate);

				strcpy(string_cfg_value, tblConfiguracoes->get("contador_timeout"));
				timeout_counter = atoi(string_cfg_value);
				printf("timeout_counter = %s \n", string_cfg_value);
				printf("timeout_counter = %d \n", timeout_counter);

				weigth_threshold = (uint32_t) ((myProduct.alert_threshold - myToken.coeff_b)/myToken.coeff_a);
				printf("weigth_threshold = %d \n", weigth_threshold);

				weigth_minimum = (uint32_t) ((myProduct.empty_weight - myToken.coeff_b)/myToken.coeff_a);
				printf("weigth_minimum = %d \n", weigth_minimum);

				protocol_server_create_get_operation_data_response((uint8_t *) &weigth_threshold,
																   (uint8_t *) &weigth_minimum,
																   (uint8_t *) &refresh_rate,
																   (uint8_t *) &timeout_counter,
																   packet_response,
																   &packet_len);

				hexdump(packet_response, packet_len);

				socket_server_connection_send((socket_server_handle_t) instance, client, (char *) packet_response, packet_len);
			}
			break;

		case CMD_ID_GET_CALIBRATION_DATA:

			protocol_server_create_get_calibration_data_response((uint8_t *) &m, (uint8_t *) &n, packet_response, &packet_len);
			break;

		case CMD_ID_NAK:

			break;

		default:

			break;
	};
}

int event_client_connected(void *instance, socket_server_client_t *client)
{
	char *ip_address;
	char msg[1024];

	ip_address = (char *) inet_ntoa((client->cli_addr).sin_addr);

	LOG_CONSOLE("New Connection from %s", ip_address);
}

void register_events(socket_server_handle_t server)
{
	socket_server_observer_t observer_on_client_connect = {
			.instance = server,
			.handler = {.client_connected = event_client_connected}
	};

	socket_server_observer_t observer_on_client_sent_message = {
			.instance = server,
			.handler = {.client_SentMessage = event_client_sent_message}
	};

	socket_server_subscribe_client_connected(server, observer_on_client_connect);
	socket_server_subscribe_client_sent_message(server, observer_on_client_sent_message);
}
