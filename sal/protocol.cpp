#include "sal/protocol.h"
#include <string.h>

#ifndef NULL
#define NULL (void *)0
#endif

typedef uint32_t (*cmd_parser_t)(uint8_t *input, size_t input_len, uint8_t *output, size_t output_len);

typedef struct
{
	uint8_t id;
	cmd_parser_t parser;
} cmd_tuple_t;

extern uint8_t packet_buffer[PROTOCOL_MAX_PACKET_SIZE];

uint32_t protocol_create_packet(uint8_t cmd_id, const uint8_t *payload, size_t payload_len, uint8_t *packet, size_t *packet_size)
{
	size_t i;
	uint8_t edc = 0x00;

	if (payload_len + 3 > PROTOCOL_MAX_PACKET_SIZE) return PROTOCOL_E_BUFFER_OVERFLOW;
	if (cmd_id >= CMD_ID_INVALID) return PROTOCOL_E_INVALID_CMD_ID;


	packet[0] = cmd_id;
	packet[1] = payload_len;
	memcpy(&packet[2], payload, payload_len);

	for(i = 0; i < payload_len + 2; i++)
	{
		edc ^= packet[i];
	}

	packet[payload_len +2] = edc;

	// CMD_ID LENGTH Payload EDC
	*packet_size = payload_len + 3;

	return PROTOCOL_E_SUCCESS;
}

uint32_t protocol_open_packet(uint8_t *bytes, size_t bytes_len, uint8_t *cmd_id, uint8_t *payload, size_t *payload_len)
{
	size_t i;
	uint8_t edc = 0x00;

	if (bytes[0] >= CMD_ID_INVALID) return PROTOCOL_E_INVALID_CMD_ID;
	if ( (bytes[1] + 3) != bytes_len) return PROTOCOL_E_INVALID_LENGTH;

	for(i = 0; i < bytes_len; i++)
	{
		edc ^= packet_buffer[i];
	}

	if(edc != bytes[bytes_len]) return PROTOCOL_E_INVALID_EDC;

	payload = &bytes[2];
	*payload_len = bytes[1];

	return PROTOCOL_E_SUCCESS;
}

uint32_t protocol_create_nak(uint8_t *packet, size_t *packet_size)
{
	return protocol_create_packet(CMD_ID_NAK, NULL, 0, packet, packet_size);
}


void swap_bytes(uint8_t *address, size_t len)
{
	size_t i;
	uint8_t tmp[len];

	memcpy(tmp, address, len);

	for(i = 0; i < len; i++) address[i] = tmp[len - i - 1];
}

uint32_t get_payload(uint8_t *packet, size_t packet_len, uint8_t **payload, size_t *payload_len)
{
	if ((packet_len <= 3) || (packet[1] == 0)) return PROTOCOL_E_NO_PAYLOAD;

	*payload = &packet[2];
	*payload_len = packet_len - 3;

	return PROTOCOL_E_SUCCESS;
}
