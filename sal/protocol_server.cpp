#include "sal/protocol_server.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BYTESWAP 1

void print_array(uint8_t *array, size_t len)
{
	size_t i = 0;
	for(i = 0; i < len; i++) printf("0x%02X ", array[i]);
}

static void get_mac_address_string(uint8_t *bytes, char *mac_address_str)
{
	sprintf(mac_address_str, "%02X:%02X:%02X:%02X:%02X:%02X", bytes[0], bytes[1], bytes[2],
															  bytes[3], bytes[4], bytes[5]);

}

int protocol_server_create_notify_response(uint8_t action, uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[1] = {action};
	if (action >= NOTIFY_ACTIONS_INVALID) return PROTOCOL_E_LOGIC;

	return protocol_create_packet(CMD_ID_NOTIFY, payload, sizeof(payload), packet, packet_len);
}

int protocol_server_parse_notify(uint8_t *packet, size_t packet_len,
								 unsigned int *sensor_value, char *mac_address_str)
{
	uint8_t *payload;
	size_t payload_len;
	uint8_t mac_address[6];
	int rv;

	rv = get_payload(packet, packet_len, &payload, &payload_len);

	if (rv) return rv;

	if (payload_len != 10) return PROTOCOL_E_INVALID_LENGTH;

	memcpy(mac_address, payload, 6);
	memcpy(sensor_value, &payload[6], 4);

	if(BYTESWAP)
	{
		swap_bytes((uint8_t *) sensor_value, sizeof(unsigned int));
		swap_bytes((uint8_t *) mac_address, sizeof(mac_address));
	}

	get_mac_address_string(mac_address, mac_address_str);


	return PROTOCOL_E_SUCCESS;
}

int protocol_server_create_get_operation_data_response(uint8_t *alert_threshold,
													   uint8_t *minimum_weight,
													   uint8_t *refresh_rate,
													   uint8_t *timeout_counter,
													   uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[16];

	if (BYTESWAP)
	{
		swap_bytes((uint8_t *) alert_threshold, 4);
		swap_bytes((uint8_t *) minimum_weight, 4);
		swap_bytes((uint8_t *) refresh_rate, 4);
		swap_bytes((uint8_t *) timeout_counter, 4);
	}

	memcpy(payload, alert_threshold, 4);
	memcpy(&payload[4], minimum_weight, 4);
	memcpy(&payload[8], refresh_rate, 4);
	memcpy(&payload[12], timeout_counter, 4);


	return protocol_create_packet(CMD_ID_GET_OPERATION_DATA, payload, sizeof(payload), packet, packet_len);
}

int protocol_server_parse_get_operation_data(uint8_t *packet, size_t packet_len,
								             char *mac_address_str)
{
	uint8_t *payload;
	size_t payload_len;
	uint8_t mac_address[6];
	int rv;

	rv = get_payload(packet, packet_len, &payload, &payload_len);

	if (rv) return rv;

	if (payload_len != 6) return PROTOCOL_E_INVALID_LENGTH;

	memcpy(mac_address, payload, 6);

	if(BYTESWAP)
	{
		swap_bytes((uint8_t *) mac_address, sizeof(mac_address));
	}

	get_mac_address_string(mac_address, mac_address_str);

	return PROTOCOL_E_SUCCESS;
}

int protocol_server_create_get_calibration_data_response(uint8_t *coeff_angular,
													   uint8_t *coeff_linear,
													   uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[8];

	memcpy(payload, coeff_angular, 4);
	memcpy(&payload[4], coeff_linear, 4);

	return protocol_create_packet(CMD_ID_GET_CALIBRATION_DATA, payload, sizeof(payload), packet, packet_len);
}
