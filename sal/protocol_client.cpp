#include "sal/protocol_client.h"
#include "string.h"

#define BYTESWAP 1

#ifndef NULL
#define NULL (void *)0
#endif

uint32_t protocol_client_create_notify(uint8_t *mac_address, uint8_t *value, uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[10];

	memcpy(payload, mac_address, 6);

	if (BYTESWAP) swap_bytes((uint8_t *) value, 4);

	memcpy(&payload[6], value, 4);

	return protocol_create_packet(CMD_ID_NOTIFY, payload, 10, packet, packet_len);
}

uint32_t protocol_client_parse_notify_response(uint8_t *packet, size_t packet_len, uint8_t *action)
{
	uint8_t *payload;
	size_t payload_len;
	uint32_t rv;

	rv = get_payload(packet, packet_len, &payload, &payload_len);
	if (rv) return rv;

	if (payload_len != 1) return PROTOCOL_E_INVALID_LENGTH;

	*action = payload[0];

	return PROTOCOL_E_SUCCESS;
}

uint32_t protocol_client_create_get_operation_data(uint8_t *mac_address, uint8_t *packet, size_t *packet_len)
{
	uint8_t payload[6];

	memcpy(payload, mac_address, sizeof(payload));

	return protocol_create_packet(CMD_ID_GET_OPERATION_DATA, payload, sizeof(payload), packet, packet_len);
}

uint32_t protocol_client_parse_get_operation_data(uint8_t *packet, size_t packet_len,
												  uint32_t *alert_threshold,
												  uint32_t *minimum_weight,
												  uint32_t *refresh_rate,
												  uint32_t *timeout_counter)
{
	uint8_t *payload;
	size_t payload_len;
	uint32_t rv;

	rv = get_payload(packet, packet_len, &payload, &payload_len);
	if (rv) return rv;

	if (payload_len != 16) return PROTOCOL_E_INVALID_LENGTH;

	memcpy(alert_threshold, payload, sizeof(uint32_t));
	memcpy(minimum_weight, &payload[4], sizeof(uint32_t));
	memcpy(refresh_rate, &payload[8], sizeof(uint32_t));
	memcpy(timeout_counter, &payload[12], sizeof(uint32_t));

	if (BYTESWAP)
	{
		swap_bytes((uint8_t *) alert_threshold, sizeof(uint32_t));
		swap_bytes((uint8_t *) minimum_weight, sizeof(uint32_t));
		swap_bytes((uint8_t *) refresh_rate, sizeof(uint32_t));
		swap_bytes((uint8_t *) timeout_counter, sizeof(uint32_t));
	}

	return PROTOCOL_E_SUCCESS;
}

uint32_t protocol_client_create_get_calibration_data(uint8_t *packet, size_t *packet_len)
{
	return protocol_create_packet(CMD_ID_GET_CALIBRATION_DATA, NULL, 0, packet, packet_len);
}

uint32_t protocol_client_parse_get_calibration_data(uint8_t *packet, size_t packet_len,
											   float *m, float *n)
{
	uint8_t *payload;
	size_t payload_len;
	uint32_t rv;

	rv = get_payload(packet, packet_len, &payload, &payload_len);
	if (rv) return rv;

	if (payload_len != 8) return PROTOCOL_E_INVALID_LENGTH;

	memcpy(m, payload, 4);
	memcpy(n, &payload[4], 4);

	return PROTOCOL_E_SUCCESS;
}
