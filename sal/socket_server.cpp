#include "sal/linked_list.h"
#include "sal/misc.h"
#include "sal/socket_server.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


struct socket_server_handle_struct
{
	int fd_server;
	struct sockaddr_in serv_addr;
	int port;
	pthread_t listening_thread;
	int max_number_of_clients;
	int active_connections;
	linked_list_t clients;
	linked_list_t subscribers_clientConnected;
	linked_list_t subscribers_clientSentMessage;
};

static void *socket_server_listening_thread(void *arg);
static void *socket_server_client_thread(void *arg);

static void socket_server_fire_client_connected_event(socket_server_handle_t connection, socket_server_client_t *client)
{
	int i, subscribers, rv;
	subscribers = linked_list_length(connection->subscribers_clientConnected);

	TRACE("Firing event: New client connected...");

	TRACE("Amount of subscribers: %d", subscribers);

	for (i = 0; i < subscribers; i++)
	{
		socket_server_observer_t observer;
		void *instance;
		rv = linked_list_get_item(connection->subscribers_clientConnected, i, (void *) &observer, sizeof(socket_server_observer_t));

		if (rv != LINKED_LIST_STATUS_SUCCESS)
		{
			TRACE("Error while recovering the observer list... 0x%04X", rv);
			exit(rv);
		}

		instance = observer.instance;
		(observer.handler).client_connected(instance, client);
	}
}

static void socket_server_fire_client_sent_message_event(socket_server_handle_t connection, socket_server_client_t *client, char *message, int len)
{
	int i, subscribers, rv;
	subscribers = linked_list_length(connection->subscribers_clientSentMessage);

	TRACE("Firing event: Client sent message...");

	TRACE("Amount of subscribers: %d", subscribers);

	for (i = 0; i < subscribers; i++)
	{
		socket_server_observer_t observer;
		void *instance;
		rv = linked_list_get_item(connection->subscribers_clientSentMessage, i, (void *) &observer, sizeof(socket_server_observer_t));

		if (rv != LINKED_LIST_STATUS_SUCCESS)
		{
			TRACE("Error while recovering the observer list... 0x%04X", rv);
			exit(rv);
		}

		instance = observer.instance;
		(observer.handler).client_SentMessage(instance, client, message, len);
	}
}

socket_server_handle_t socket_server_connection_create(int number_of_clients, int port)
{
	socket_server_handle_t connection;

	connection = (socket_server_handle_t) malloc(sizeof(struct socket_server_handle_struct));
	bzero(connection, sizeof(struct socket_server_handle_struct));

	connection->max_number_of_clients = number_of_clients;
	connection->port = port;
	connection->clients = linked_list_create();
	connection->active_connections = 0;
	connection->subscribers_clientConnected = linked_list_create();
	connection->subscribers_clientSentMessage = linked_list_create();

	TRACE("Creating thread %d...", 1);
	if (pthread_create(&(connection->listening_thread), NULL, socket_server_listening_thread, connection)){
		TRACE("Failed to create listening thread...");
		exit(1);
	}

	TRACE("Thread: startListening ON");


	return connection;
}

int socket_server_connection_close(socket_server_handle_t connection)
{
	return SOCKET_SERVER_STATUS_SUCCESS;
}


int socket_server_connection_send(socket_server_handle_t connection, const socket_server_client_t *client, const char *message, const size_t len)
{
	int rv = 0x00;
	TRACE("Sending to client (%d): %s", len, message);

	rv = write(client->fd, message, len);

	return rv;
}

int socket_server_subscribe_client_connected(socket_server_handle_t connection, socket_server_observer_t listener)
{
	return linked_list_add(connection->subscribers_clientConnected, (void *) &listener, sizeof(listener));
}

int socket_server_subscribe_client_sent_message(socket_server_handle_t connection, socket_server_observer_t listener)
{
	return linked_list_add(connection->subscribers_clientSentMessage, (void *) &listener, sizeof(listener));
}

int socket_server_get_port(socket_server_handle_t connection)
{
	return connection->port;
}


static void *socket_server_listening_thread(void *arg){
	int c;
	char error_message[1024];
	socket_server_handle_t connection;
	connection = (socket_server_handle_t) arg;

	if ((connection->fd_server = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		switch(errno){
			case EACCES:
					TRACE("Permission denied while creating the socket \n");
				break;

			case EAFNOSUPPORT:
					TRACE("This implementation does not support the specified address family \n");
				break;

			case EINVAL:
					TRACE("Unknown protocol or family not available.. \n");
				break;

			case EMFILE:
					TRACE("Process file table overflow \n");
				break;

			case ENFILE:
					TRACE("The system limit on the total number of open files has been reached \n");
				break;

			case ENOBUFS:
			case ENOMEM:
					TRACE("Insufficient memory \n");
				break;

			default:
					TRACE("Unknown \n");
				break;
		}
		exit(1);
	}

	// Initializes and fill the server socket structure (needed by bind)
	bzero(&(connection->serv_addr), sizeof(connection->serv_addr));

	connection->serv_addr.sin_family = AF_INET;
	connection->serv_addr.sin_addr.s_addr = INADDR_ANY;
	connection->serv_addr.sin_port = htons(connection->port);

	if (bind(connection->fd_server, (struct sockaddr *) &(connection->serv_addr), sizeof(connection->serv_addr)) < 0){
		switch(errno){
			case EACCES:
					TRACE("Permission denied while creating the socket \n");
				break;

			case EADDRINUSE:
					TRACE("The address is already in use (port already in use?) \n");
				break;

			case EINVAL:
					TRACE("The socket is already bound to an address \n");
				break;

			case ENOBUFS:
			case ENOMEM:
					TRACE("Insufficient memory \n");
				break;

			default:
					TRACE("Unknown \n");
				break;
		}
		exit(1);
	}

	listen(connection->fd_server, 3);

	c = sizeof(struct sockaddr_in);

	TRACE("Waiting for incoming connections... \n");

	while(1){
		connection->active_connections = linked_list_length(connection->clients);

		if (connection->active_connections < connection->max_number_of_clients){
			int number_connections = connection->active_connections;
			int fd_server = connection->fd_server;

			// Allocate a client
			socket_server_client_t *client;
			client = (socket_server_client_t *) malloc(sizeof(socket_server_client_t));

			client->fd = accept(fd_server, (struct sockaddr *) &(client->cli_addr), (socklen_t *) &c);
			linked_list_add(connection->clients, (void *) client, sizeof(socket_server_client_t));

			TRACE("A new client connected.. now we have %d/%d clients", connection->active_connections, connection->max_number_of_clients);

			socket_server_fire_client_connected_event(connection, client);

			// Create thread to handle this client
			client->connection = (void *) connection;
			if (pthread_create(&(client->thread), NULL, socket_server_client_thread, (void *) client)){
					TRACE("Failed to create client thread...");
					exit(1);
			}
		}
	}
}

static void *socket_server_client_thread(void *args){
	socket_server_client_t *client = (socket_server_client_t *) args;
	socket_server_handle_t server_handle = (socket_server_handle_t) client->connection;

	int read_size;
	while( (read_size = recv(client->fd, client->recv_buffer, SOCKET_SERVER_CLIENT_BUFFERS_SIZE , 0)) > 0 )
	{
		// Trigger event
		socket_server_fire_client_sent_message_event((socket_server_handle_t) client->connection, client, client->recv_buffer, read_size);

		// Clears the reception buffer :)
		memset(client->recv_buffer, '\0', SOCKET_SERVER_CLIENT_BUFFERS_SIZE);
	}

	TRACE("Client disconnected..");
	linked_list_remove(server_handle->clients, (void *) client, sizeof(socket_server_client_t));
	server_handle->active_connections--;
	TRACE("A client disconected.. now we have %d/%d clients", server_handle->active_connections, server_handle->max_number_of_clients);
}
