set (SOURCE_FILES 
		protocol.cpp
		protocol_client.cpp
		protocol_server.cpp
		socket_server.cpp
		linked_list.cpp
)

#include_directories(sal ${CMAKE_SOURCE_DIR}/inc)

add_library (sal ${SOURCE_FILES})
