#include "sal/linked_list.h"
#include "sal/misc.h"

#include <string.h>
#include <stdlib.h>

typedef struct node_struct{
	void *item;
	struct node_struct *next;
} linked_list_node_t;

typedef struct linked_list_head_t{
	linked_list_node_t head_node;
	compareMethod compare_method;
	printMethod print_method;
	int size;
} linked_list_head_t;

static int linked_list_default_compare_method(void *a, void *b, unsigned int memsize)
{
	if ((a == NULL) || (b == NULL) || (memsize == 0))
	{
		return LINKED_LIST_STATUS_COMPARISON_FAILED;
	}

	if (memcmp(a, b, memsize) == 0)
	{
		return LINKED_LIST_STATUS_SUCCESS;
	}else{
		return LINKED_LIST_STATUS_COMPARISON_FAILED;
	}
}

static void linked_list_default_print_method(void *item)
{
	int *pitem = (int *) item;
	printf("%d \n", *pitem);
}


linked_list_t linked_list_create()
{
	linked_list_head_t *initial_node;

	TRACE("Creating linked list on the heap...");
	initial_node = (linked_list_head_t *) malloc(sizeof(linked_list_head_t));

	TRACE("Top node alloced...");
	bzero(initial_node, sizeof(linked_list_head_t));

	initial_node->head_node.next = NULL;

	TRACE("Filling structure with the default methods...");

	initial_node->compare_method = linked_list_default_compare_method;
	TRACE("Setted the default comparison method...");

	initial_node->print_method = linked_list_default_print_method;
	TRACE("Setted the default print method...");

	(initial_node->head_node).next = NULL;
	TRACE("Setted next node as NULL...");

	TRACE("Returning...");
	return initial_node;
}

int linked_list_add(linked_list_t list, void *item, unsigned int item_len)
{
	linked_list_head_t *head;
	linked_list_node_t *node, *last;

	head = (linked_list_head_t *) list;
	node = & (head->head_node);

	TRACE("The list has %d elements...", head->size);
	TRACE("Adding an item to the linked list...");

	while(node->next != NULL)
	{
		TRACE("Iterating...");
		node = node->next;
	}

	TRACE("Before malloc...");
	last = (linked_list_node_t *) malloc(sizeof(linked_list_node_t));
	TRACE("After malloc...");

	last->next = NULL;
	last->item = malloc(item_len);

	TRACE("Before copy...");
	memcpy(last->item, item, item_len);
	TRACE("After copy...");

	node->next = last;

	head->size++;

	return LINKED_LIST_STATUS_SUCCESS;
}

int linked_list_remove(linked_list_t list, void *item, unsigned int memsize)
{
	linked_list_head_t *head;
	linked_list_node_t *node;;
	linked_list_node_t *to_be_excluded;

	head = (linked_list_head_t *) list;
	node = & (head->head_node);

	// If the requested item is the head of the linked list
	if (head->compare_method(item, node->item, memsize) == LINKED_LIST_STATUS_SUCCESS)
	{
		if (node->next == NULL)
		{
			return LINKED_LIST_STATUS_SUCCESS;
		}

		memcpy(&head->head_node, node->next, sizeof(linked_list_node_t));
		free(node->next->item);
		free(node->next);

		return LINKED_LIST_STATUS_SUCCESS;
	}
	while(!head->compare_method(item, node->next->item, memsize)) node = node->next;

	to_be_excluded = node->next;

	// Bypasses the one we want to exclude
	node->next = to_be_excluded->next;

	free(to_be_excluded->item);
	free(to_be_excluded);

	head->size--;

	return LINKED_LIST_STATUS_SUCCESS;
}

int linked_list_destroy(linked_list_t list)
{
	linked_list_head_t *head;
	linked_list_node_t *to_be_removed, *node;

	head = (linked_list_head_t *) list;
	node = & (head->head_node);

	while(node->next != NULL)
	{
		to_be_removed = node->next;

		node->next = to_be_removed->next;

		free(to_be_removed->item);
		free(to_be_removed);
	}
}

int linked_list_set_compare_method(linked_list_t list, compareMethod method)
{
	linked_list_head_t *head;
	head = (linked_list_head_t *) list;

	head->compare_method = method;

	return LINKED_LIST_STATUS_SUCCESS;
}

int linked_list_set_print_method(linked_list_t list, printMethod method)
{
	linked_list_head_t *head;
	head = (linked_list_head_t *) list;

	head->print_method = method;
}

void linked_list_exec_all(linked_list_t list)
{
	linked_list_node_t *node;
	linked_list_head_t *head;

	head = (linked_list_head_t *) list;
	node = & (head->head_node);

	while(node->next != NULL)
	{
		node = node->next;
		head->print_method(node->item);
	}
}

int linked_list_length(linked_list_t list)
{
	linked_list_head_t *head;
	head = (linked_list_head_t *) list;

	return head->size;
}

int linked_list_get_item(linked_list_t list, int pos, void *item, int length)
{
	linked_list_head_t *head;
	linked_list_node_t *node;

	head = (linked_list_head_t *) list;
	node = & (head->head_node);

	if ((pos > head->size) || (pos < 0))
	{
		TRACE("Received an invalid position");
		item = NULL;
		return LINKED_LIST_STATUS_ELEMENT_NOT_FOUND;
	}

	pos++;

	while(pos--){
		node = node->next;
		TRACE("Iterating... ");
	}

	memcpy(item, node->item, length);

	TRACE("Returning... ");

	return LINKED_LIST_STATUS_SUCCESS;
}
